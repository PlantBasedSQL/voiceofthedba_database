IF OBJECT_ID('[dbo].[SomeNewProc]') IS NOT NULL
	DROP PROCEDURE [dbo].[SomeNewProc];

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--SET QUOTED_IDENTIFIER ON|OFF
--SET ANSI_NULLS ON|OFF
--GO
CREATE PROCEDURE [dbo].[SomeNewProc]
    @parameter_name AS INT
-- WITH ENCRYPTION, RECOMPILE, EXECUTE AS CALLER|SELF|OWNER| 'user_name'
AS
    BEGIN
        SELECT ContactsID ,
               ContactFullName ,
               PhoneWork ,
               PhoneMobile ,
               Address1 ,
               Address2 ,
               Address3 ,
               CountryCode ,
               JoiningDate ,
               ModifiedDate ,
               Email ,
               Photo ,
               LinkedIn
        FROM   dbo.Contacts;
    END;
GO
