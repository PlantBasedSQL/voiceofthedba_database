IF OBJECT_ID('[dbo].[GetArticles]') IS NOT NULL
	DROP PROCEDURE [dbo].[GetArticles];

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--SET QUOTED_IDENTIFIER ON|OFF
--SET ANSI_NULLS ON|OFF
--GO
create procedure [dbo].[GetArticles] @parameter_name as int
-- WITH ENCRYPTION, RECOMPILE, EXECUTE AS CALLER|SELF|OWNER| 'user_name'
as
begin
    select ArticlesID
         , AuthorID
         , Title
         , Description
         , Article
         , PublishDate
         , ModifiedDate
         , URL
         , Comments
    from dbo.Articles;
end;
GO
