IF OBJECT_ID('[dbo].[HappyLittleProc]') IS NOT NULL
	DROP PROCEDURE [dbo].[HappyLittleProc];

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--SET QUOTED_IDENTIFIER ON|OFF
--SET ANSI_NULLS ON|OFF
--GO
CREATE PROCEDURE [dbo].[HappyLittleProc]
    @parameter_name AS INT
-- WITH ENCRYPTION, RECOMPILE, EXECUTE AS CALLER|SELF|OWNER| 'user_name'
AS
    BEGIN
        SELECT ArticlePricesID ,
               ArticlesID ,
               Price ,
               ValidFrom ,
               ValidTo ,
               Active ,
               SalesPrice
        FROM   dbo.ArticlePrices;
    END;
GO
